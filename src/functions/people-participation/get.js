const { logErrors } = require('../../helpers')
const { DynamoDB } = require('../../config/aws')
const response = require('../../handlers/response')
const errorHandling = require('../../handlers/error')

/**
 * @typedef {import('aws-lambda').APIGatewayProxyResult} APIGatewayProxyResult
 */

/**
 * @returns {Promise<APIGatewayProxyResult>}
 */
const handler = async event => {

  const dynamoDb = new DynamoDB()

  try {

    const tableName = process.env.DYNAMODB_TABLE || "cubobackend-jest"

    const result = await dynamoDb.scan({
      TableName: tableName
    })

    return response({
      body: result,
      cors: true
    })

  } catch (error) {
    
    await logErrors('Get', error)
    const err = await errorHandling(error)

    return response({
      statusCode: err.statusCode,
      body: err.payload,
      cors: true
    })
  }
}

module.exports = handler
