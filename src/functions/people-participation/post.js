const { v4: uuidv4 } = require('uuid');
const { logErrors, stringValidation } = require('../../helpers')
const { DynamoDB } = require('../../config/aws')
const response = require('../../handlers/response')
const errorHandling = require('../../handlers/error')
const Joi = require('@hapi/joi');

/**
 * @typedef {import('aws-lambda').APIGatewayProxyEvent} APIGatewayProxyEvent
 * @typedef {import('aws-lambda').APIGatewayProxyResult} APIGatewayProxyResult
 */

/**
 * @typedef RequestBody
 * @property {string} firstName
 * @property {string} lastName
 * @property {number} participation
 */

/**
 * Recebe o payload na estrutura básica.
 *
 * @param {APIGatewayProxyEvent} event
 * @returns {Promise<APIGatewayProxyResult>}
 */
const handler = async event => {

  const tableName = process.env.DYNAMODB_TABLE || "cubobackend-jest"
  const dynamoDb = new DynamoDB()
  const schemaJson = Joi.object({
    "cuboId": Joi.string().optional(),
    "firstName": Joi.string().required(),
    "lastName": Joi.string().required(),
    "participation": Joi.number().min(0.01).max(100).required(),
  })

  try {
    // Basic validation
    if (stringValidation(event.body) === false) {
      return response({
        statusCode: 400,
        body: { message: "Invalid Body" },
        cors: true
      })
    }

    /** @type {RequestBody} */
    const payload = JSON.parse(event.body)

    // Validation Struct
    const { error } = schemaJson.validate(payload)
    if (error != undefined) {
      return response({
        statusCode: 400,
        body: { message: "Invalid Json" },
        cors: true
      })
    }

    // Validation = Greater than 100
    const result = await dynamoDb.scan({
      TableName: tableName
    })

    if (result !== undefined && result.length > 0){
      const checkSum = await result.reduce((x,y) => {
        return x + y.participation
      }, 0)
      if (checkSum + payload.participation > 100){
        return response({
          statusCode: 400,
          body: { message: "Sum participation greater then 100%" },
          cors: true
        })
      }
    }

    await dynamoDb.put({
      TableName : tableName,
      Item: {
        cuboId: uuidv4(),
        firstName: payload.firstName,
        lastName: payload.lastName,
        participation: payload.participation
      }
    })

    return response({
      body: { message: 'Saved', bodyToSave: payload },
      cors: true
    })

  } catch (error) {
    await logErrors('Post', error)
    const err = await errorHandling(error)

    return response({
      statusCode: err.statusCode,
      body: err.payload,
      cors: true
    })
  }
}

module.exports = handler
