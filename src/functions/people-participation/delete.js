const { logErrors } = require('../../helpers')
const { DynamoDB } = require('../../config/aws')
const response = require('../../handlers/response')
const errorHandling = require('../../handlers/error')

/**
 * @typedef {import('aws-lambda').APIGatewayProxyResult} APIGatewayProxyResult
 */

/**
 * @returns {Promise<APIGatewayProxyResult>}
 */
const handler = async event => {
  try {

    const tableName = process.env.DYNAMODB_TABLE || "cubobackend-jest"
    const dynamoDb = new DynamoDB()

    // Validation = Greater than 100
    const result = await dynamoDb.scan({
      TableName: tableName
    })

    for (element in result){
      await dynamoDb.delete({
        TableName : tableName,
        Key: {
          "cuboId": element.cuboId
        }
      });
    }

    return response({
      body: {},
      cors: true
    })

  } catch (error) {
    await logErrors('Delete', error)
    const err = await errorHandling(error)

    return response({
      statusCode: err.statusCode,
      body: err.payload
    })
  }
}

module.exports = handler
