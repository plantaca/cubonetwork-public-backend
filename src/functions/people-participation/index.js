const response = require('../../handlers/response')

const getHandler    = require('./get')
const postHandler   = require('./post')
const deleteHandler = require('./delete')

/**
 * @typedef {import('aws-lambda').APIGatewayProxyEvent} APIGatewayProxyEvent
 * @typedef {import('aws-lambda').APIGatewayProxyResult} APIGatewayProxyResult
 */

/**
 * @param {APIGatewayProxyEvent} event
 * @returns {Promise<APIGatewayProxyResult>}
 */
const handler = async event => {
  if (event.httpMethod === 'POST') {
    return postHandler(event)
  } else if (event.httpMethod === 'DELETE') {
    return deleteHandler(event)
  } else if (event.httpMethod === 'GET') {
    return getHandler(event)
  } else {
    return response({
      body: {
        message: 'This resource is not available'
      },
      statusCode: 403
    })
  }
}

module.exports.handler = handler