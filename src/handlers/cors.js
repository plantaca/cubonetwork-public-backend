/**
 * Adiciona a propriedade `cors` como parte dos parâmetros.
 *
 * Quando a propriedade é `true`, adiciona os cabeçalhos de
 * CORS para autorizar qualquer domínio. Quando a propriedade
 * é um objeto, adiciona a propriedade como parte dos cabeçalhos.
 *
 * @template {function} T
 * @param {T} response
 * @returns {T}
 */
module.exports = response => ({ statusCode, body, headers, cors }) => {
  headers = headers || {}

  if (typeof cors === 'object' && cors != null) {
    headers = { ...headers, ...cors }
  } else if (cors) {
    headers = { ...headers, ...{ 'Access-Control-Allow-Origin': '*' } }
  }

  return response({ statusCode, body, headers })
}
