const corsInjector = require('./cors')

/**
 * @typedef {import("aws-lambda").APIGatewayProxyResult} APIGatewayProxyResult
 * @typedef {import("aws-lambda").APIGatewayProxyResult["statusCode"]} StatusCode
 * @typedef {import("aws-lambda").APIGatewayProxyResult["headers"]} Headers
 */

/**
 * Cria uma resposta de API Gateway a partir dos parâmetros
 *
 * @param {object} params
 * @param {object} params.body objeto a ser retornado na resposta
 * @param {StatusCode} [params.statusCode=200] código de status da resposta
 * @param {Headers} [params.headers] cabeçalhos da resposta
 *
 * @returns {APIGatewayProxyResult} resposta apropriada
 */
const response = ({ statusCode = 200, body, headers }) => {
  return {
    statusCode,
    headers,
    body: typeof body === 'string' ? body : JSON.stringify(body, null, 2)
  }
}

module.exports = corsInjector(response)
