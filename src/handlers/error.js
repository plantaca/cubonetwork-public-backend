const errorStatus = require('../enums/errorStatus')

/**
 * @typedef {import("aws-lambda").APIGatewayProxyResult} APIGatewayProxyResult
 */

/**
 * Cria uma resposta de erro para o API Gateway
 *
 * @param {Error} error erro a ser analisado
 *
 * @returns {APIGatewayProxyResult} resposta apropriada
 */
const errorHandler = async error => {
  const errorExternal = ['RequestError', 'IntegrationError'].includes(
    error.constructor.name
  )

  if (errorExternal) {
    return {
      statusCode: error.status,
      payload: { message: `${error.message} :(` }
    }
  }

  return {
    statusCode: errorStatus.INTERNAL_SERVER_ERROR,
    payload: { message: 'Ocorreu um erro inesperado :(' }
  }
}

module.exports = errorHandler
