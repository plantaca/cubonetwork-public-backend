/**
 * Deprecated - Exibe mensagem de erro
 *
 * @param {any} value Valor para validar
 */
const numberValidation = (value) => {
  if (value !== undefined && 
      typeof value === "number"){
    return true
  }else{
    return false
  }
}

module.exports = numberValidation
