/**
 * Exibe mensagem de erro
 *
 * @param {string} text Texto para validar
 */
const stringValidation = (text) => {
  if (text !== undefined && text.trim() !== ""){
    return true
  }else{
    return false
  }
}

module.exports = stringValidation
