const logErrors = require('./logs')
const stringValidation = require('./stringValidation')

module.exports = {
  logErrors,
  stringValidation
}
