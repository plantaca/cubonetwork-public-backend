const winston = require('winston')

/**
 * Exibe mensagem de erro
 *
 * @param {string} service nome do serviço que teve erro
 * @param {Error} error objeto de erro a ser exibido
 */
const logErrors = async (service, error) => {
  const logger = winston.createLogger({
    transports: [new winston.transports.Console()]
  })

  logger.error(
    JSON.stringify({
      environment: process.env.NODE_ENV,
      error: error.stack,
      service
    })
  )
}

module.exports = logErrors
