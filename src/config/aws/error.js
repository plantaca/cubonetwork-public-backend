/**
 * @param {string} message
 * @param {Error} error
 * @param {string|number} [code]
 * @param {number} [statusCode]
 */
const customError = (message, error, code, statusCode) => {
    // Make message always enumerable for serialization
    Object.defineProperty(error, 'message', {
      enumerable: true
    })
    const customError = new Error(
      message.concat('. Error JSON: ', JSON.stringify(error))
    )
  
    customError.code = code || error.code
    customError.statusCode = statusCode || error.statusCode
  
    return customError
  }
  
  module.exports = { customError }
  