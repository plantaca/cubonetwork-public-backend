const { DocumentClient } = require('aws-sdk/clients/dynamodb')
const { customError } = require('./error')

/**
 * @typedef {import('aws-sdk').DynamoDB.Types.ClientConfiguration} ClientConfiguration
 * @typedef {import('aws-sdk').DynamoDB.DocumentClient.DocumentClientOptions} DocumentClientConfiguration
 * @typedef {ClientConfiguration & DocumentClientConfiguration} Configuration
 *
 * @typedef {import('aws-sdk').DynamoDB.DocumentClient.GetItemInput} GetItemInput
 * @typedef {import('aws-sdk').DynamoDB.DocumentClient.GetItemOutput["Item"]} GetItemOutput
 *
 * @typedef {import('aws-sdk').DynamoDB.DocumentClient.QueryInput} QueryInput
 * @typedef {import('aws-sdk').DynamoDB.DocumentClient.QueryOutput["Items"]} QueryOutput
 * 
 * @typedef {import('aws-sdk').DynamoDB.DocumentClient.ScanOutput["Items"]} ScanOutput
 *
 * @typedef {import('aws-sdk').DynamoDB.DocumentClient.PutItemInput} PutItemInput
 *
 * @typedef {import('aws-sdk').DynamoDB.DocumentClient.UpdateItemInput} UpdateItemInput
 * @typedef {import('aws-sdk').DynamoDB.DocumentClient.UpdateItemOutput["Attributes"]} UpdateItemOutput
 * 
  * @typedef {import('aws-sdk').DynamoDB.DocumentClient.DeleteItemInput} DeleteItemInput
 */

class DynamoDB {
    /**
     * Creates a DynamoDB client. The client behaves similar to
     * DynamoDB.DocumentClient of AWS SDK, but it is Promise-oriented.
     *
     * If `apiVersion` is not set in the configuration, it will default
     * to `2012-08-10`.
     *
     * The constructor also supports some configuration to be passed
     * through the environment. Set `DYNAMODB_ENDPOINT` to configure
     * the default endpoint and `DYNAMODB_REGION` to configure the
     * default region.
     *
     * The configuration passed as argument takes precedence over
     * environment variables.
     *
     * @param {Configuration} config
     */
    constructor (config) {
      const isTest = process.env.JEST_WORKER_ID;

      const defaults = {
        apiVersion: '2012-08-10'
      }
      const envConfig = {}
      if (process.env.DYNAMODB_ENDPOINT) {
        envConfig.endpoint = process.env.DYNAMODB_ENDPOINT
      }
      if (process.env.DYNAMODB_REGION) {
        envConfig.region = process.env.DYNAMODB_REGION
      } else if (process.env.REGION_AWS) { // backward compatibility
        envConfig.region = process.env.REGION_AWS
      }

      if (isTest){
        envConfig.endpoint = 'localhost:8000';
        envConfig.sslEnabled = false;
        envConfig.region = 'local-env';
      }
  
      /** @private */
      this.ddb = new DocumentClient({
        ...defaults,
        ...envConfig,
        ...config
      })
    }
  
    /**
     * @param {QueryInput} params
     * @returns {Promise<QueryOutput>}
     */
    async query (params) {
      let data
  
      try {
        data = await this.ddb.query(params).promise()
      } catch (error) {
        throw customError('Unable to query items', error)
      }
  
      return data.Items
    }

    /**
     * @returns {Promise<ScanOutput>}
     */
    async scan (params) {
      let data
  
      try {
        data = await this.ddb.scan(params).promise()
      } catch (error) {
        throw customError('Unable to scan items', error)
      }
  
      return data.Items
    }
  
    /**
     * @param {PutItemInput} params
     * @returns {Promise<void>}
     */
    async put (params) {
      try {
        await this.ddb.put(params).promise()
      } catch (error) {
        throw customError('Unable to add item', error)
      }
    }

        /**
     * @param {DeleteItemInput} params
     * @returns {Promise<void>}
     */
    async delete (params) {
      try {
        await this.ddb.delete(params).promise()
      } catch (error) {
        throw customError('Unable to add item', error)
      }
    }
  
  }
  
  module.exports = DynamoDB
  