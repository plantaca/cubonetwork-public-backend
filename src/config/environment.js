require('dotenv').config()

module.exports.config = async () => {
  return {
    region: process.env.AWS_REGION
  }
}
