module.exports = {
  tables: [
    {
      TableName: 'cubobackend-jest',
      KeySchema: [{AttributeName: 'cuboId', KeyType: 'HASH'}],
      AttributeDefinitions: [{AttributeName: 'cuboId', AttributeType: 'S'}],
      ProvisionedThroughput: {ReadCapacityUnits: 1, WriteCapacityUnits: 1}
    }
  ],
  port: 8000
};