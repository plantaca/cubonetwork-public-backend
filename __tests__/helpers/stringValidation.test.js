const stringValidation = require('../../src/helpers/stringValidation')

describe('String Validations Utils', () => {

    it('should if parameters is valid', () =>{
        expect(stringValidation("texto")).toBeTruthy()
    });

    describe('Negative tests', () => {
        
        it('should if parameters is undefined', () =>{
            expect(stringValidation(undefined)).toBeFalsy()
        });

        it('should if parameters is string empty', () =>{
            expect(stringValidation("")).toBeFalsy()
        });

        it('should if parameters is string blank space', () =>{
            expect(stringValidation(" ")).toBeFalsy()
        });

    });

});