const { DocumentClient } = require('aws-sdk/clients/dynamodb');
const peopleGet = require('../../src/functions/people-participation/get')

describe('peopleGet Validation', () => {

    beforeAll( () =>{
        const config = {
            convertEmptyValues: true,
            endpoint: 'localhost:8000', sslEnabled: false, region: 'local-env'}
        const ddb = new DocumentClient(config)
        ddb.put({TableName: '"cubobackend-jest"', Item: {cuboId: '1', firstName: 'Nome 1', lastName: 'Nome 1', 'participation': 50}})
    })

    it('should if parameters is valid', () =>{
        
        const event = {};
        peopleGet(event).then(result =>{
            expect(result).toBeTruthy()
            expect(result.body).toBeDefined()
            expect(result.body.length).toBeGreaterThan(1)
        })

    });
});