const { DocumentClient } = require('aws-sdk/clients/dynamodb');
const peoplePost = require('../../src/functions/people-participation/post')

describe('peopleGet Validation', () => {

    it('should if parameters is valid', () =>{
        
        const event = {
            body: '{"firstName": "Primeiro nome", "lastName": "Segundo Nome", "participation": 50}'
        };
        peoplePost(event).then(result =>{
            expect(result).toBeTruthy()
            expect(result.body).toBeDefined()
            expect(result.body).toMatch("Saved")
        })

    });

    describe('Negative tests', () => {
        it('should not payload wrong', () =>{
        
            const event = {
                body: '{"Name": "Primeiro nome", "lastName": "Segundo Nome", "participation": 50}'
            };
            peoplePost(event).then(result =>{
                expect(result).toBeTruthy()
                expect(result.body).toBeDefined()
                expect(result.body).not.toMatch("Saved")
            })
    
        });

        it('should not save 1000', () =>{
        
            const event = {
                body: '{"firstName": "Primeiro nome", "lastName": "Segundo Nome", "participation": 1000}'
            };
            
            peoplePost(event).then(result =>{
                expect(result).toBeTruthy()
                expect(result.body).toBeDefined()
                expect(result.body).not.toMatch("Saved")
            })
    
        });

    });
});